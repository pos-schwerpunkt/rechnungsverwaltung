CREATE DATABASE if not exists rever CHAR SET = utf8; /* rever = ReVer = Rechnungs-Verwaltung */
USE rever;

CREATE TABLE if not EXISTS tbl_invoices (
iID INTEGER NOT NULL AUTO_INCREMENT,
iDate DATETIME NOT NULL,
iCustomer VARCHAR(255) NOT NULL,
iDescription VARCHAR(255) NOT NULL,
iValue DOUBLE NOT NULL,
iPaid BOOLEAN NOT NULL,
unique(iCustomer, iDate, iPaid),
PRIMARY KEY(iID)
);

INSERT INTO tbl_invoices(iDate, iCustomer, iDescription, iValue, iPaid)
VALUES ('2020-01-07', 'John Doe', 'Software Purchase', '540.6', false);

INSERT INTO tbl_invoices(iDate, iCustomer, iDescription, iValue, iPaid)
VALUES ('2020-01-07', 'Max Mustermann', 'Hardware Purchase', '1547.85', true);

INSERT INTO tbl_invoices(iDate, iCustomer, iDescription, iValue, iPaid)
VALUES ('2020-01-07', 'Hans Bauer', 'Office Purchase', '47.80', false);

SELECT * FROM tbl_invoices;

DELETE FROM tbl_invoices WHERE iCustomer = 'John Doe';

DROP DATABASE rever;