package at.itkolleg;

import java.sql.Date;

public class Invoice {
    private int iID;
    private Date iDate;
    private String iDescription;
    private String iCustomer;
    private Double iValue;
    private Boolean iPaid;

    public Invoice(Date iDate, String iDescription, String iCustomer, Double iValue, Boolean iPaid) {
        this.iDate = iDate;
        this.iDescription = iDescription;
        this.iCustomer = iCustomer;
        this.iValue = iValue;
        this.iPaid = iPaid;
    }

    public int getiID() {
        return iID;
    }

    public void setiID(int iID) {
        this.iID = iID;
    }

    public Date getiDate() {
        return iDate;
    }

    public  void setiDate(Date iDate) {
        this.iDate = iDate;
    }

    public  String getiDescription() {
        return iDescription;
    }

    public  void setiDescription(String iDescription) {
        this.iDescription = iDescription;
    }

    public  String getiCustomer() {
        return iCustomer;
    }

    public  void setiCustomer(String iCustomer) {
        this.iCustomer = iCustomer;
    }

    public  Double getiValue() {
        return iValue;
    }

    public  void setiValue(Double iValue) {
        this.iValue = iValue;
    }

    public  Boolean getiPaid() {
        return iPaid;
    }

    public  void setiPaid(Boolean iPaid) {
        this.iPaid = iPaid;
    }

    @Override
    public String toString() {
        return "Invoice from " + iDate +
                " about " + iDescription +
                " from " + iCustomer +
                " with " + iValue +
                "$ is paid: " + iPaid;
    }
}
