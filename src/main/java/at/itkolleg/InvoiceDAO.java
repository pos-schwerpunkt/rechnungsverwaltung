package at.itkolleg;

import java.util.ArrayList;

public interface InvoiceDAO {
    public ArrayList<Invoice> showInvoice();
    public void insertInvoice(Invoice i);
    public void updateInvoice(int id, Invoice i);
    public void deleteInvoice(int id);
}
