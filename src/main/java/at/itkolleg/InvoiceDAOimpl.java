package at.itkolleg;

import java.sql.Date;
import java.util.ArrayList;

public class InvoiceDAOimpl implements InvoiceDAO {

    private ArrayList<Invoice> invoice;

    public InvoiceDAOimpl() {
        invoice = new ArrayList<Invoice>();
        long millis = System.currentTimeMillis();
        Date d = new java.sql.Date(millis);
        this.invoice.add(new Invoice(d, "Client 1", "Sample Purchase 1", 100.00, false));
        this.invoice.add(new Invoice(d, "Client 2", "Sample Purchase 2", 200.00, true));
        this.invoice.add(new Invoice(d, "Client 3", "Sample Purchase 3", 300.00, false));
    }

    @Override
    public ArrayList<Invoice> showInvoice() {
        return this.invoice;
    }

    @Override
    public void insertInvoice(Invoice i) {
        if(i!=null) {
            this.invoice.add(i);
        }
    }

    @Override
    public void updateInvoice(int id, Invoice i) {
        for(Invoice in : this.invoice) {
            if(in.getiID()== id) {
                this.invoice.set(id, i);
                return;
            }
        }
    }

    @Override
    public void deleteInvoice(int id) {
        for(Invoice in : this.invoice) {
            if(in.getiID()== id) {
                this.invoice.remove(in);
                return;
            }
        }
    }
}
