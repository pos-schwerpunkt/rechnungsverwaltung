package at.itkolleg;

import java.sql.*;

public class App {
    private static final String DRIVER = "com.mysql.cj.jdbc.Driver";
    private static final String URL = "jdbc:mysql://127.0.0.1:3306/rever";
    private static final String USER = "root";
    private static final String PASS = "";
    private static final String SELECT = "SELECT * FROM tbl_invoices";
    private static final String INSERT = "INSERT INTO tbl_invoices VALUES (NULL, ?, ?, ?, ?, ?)";
    private static final String UPDATE = "UPDATE tbl_invoices SET iDate=?, iCustomer=?, iDescription=?, iValue=?, iPaid=? WHERE iID=?";
    private static final String DELETE = "DELETE FROM tbl_invoices WHERE iID =?";

    public static void main(String[] args) {
        showInvoice();
        long millis = System.currentTimeMillis();
        Date d = new java.sql.Date(millis);
        insertInvoice(d, "Franz Muster", "Special purchase", 500.87, false);
        showInvoice();
        updateInvoice(4, d, "Josef Muster", "Cat Software", 500.87, true);
        showInvoice();
        deleteInvoice(4);
        showInvoice();
        /*
        InvoiceDAO invoiceDao = new InvoiceDAOimpl();
        ArrayList<Invoice> liste = invoiceDao.showInvoice();
        for(Invoice i : liste) {
            System.out.println(i);
        }
         */
    }

    public static Statement Statement() throws SQLException, ClassNotFoundException {
        Class.forName(DRIVER);
        Connection con = DriverManager.getConnection(URL, USER, PASS);
        Statement stmt = con.createStatement();
        return stmt;
    }

    public static Connection Connection() throws SQLException {
        Connection con = DriverManager.getConnection(URL, USER, PASS);
        return con;
    }

    public static void showInvoice() {
        try {
            ResultSet rs = Statement().executeQuery(SELECT);
            ResultSetMetaData meta = rs.getMetaData();
            int col = meta.getColumnCount();
            String header = "";
            for (int i = 1; i <= col; i++) {
                System.out.print(meta.getColumnName(i) + "\t\t");
            }
            System.out.println(header);
            while (rs.next())
                System.out.println(rs.getInt(1) + "\t" + rs.getDate(2) + "\t" + rs.getString(3)
                        + "\t" + rs.getString(4) + "\t" + rs.getDouble(5) + "\t" + rs.getBoolean(6));
            Connection().close();
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    public static void insertInvoice(Date date, String customer, String description, double value, Boolean paid) {
        try {
            PreparedStatement ps = Connection().prepareStatement(INSERT);
            ps.setDate(1, date);
            ps.setString(2, customer);
            ps.setString(3, description);
            ps.setDouble(4, value);
            ps.setBoolean(5, paid);
            ps.executeUpdate();
        } catch (SQLIntegrityConstraintViolationException e) {
            System.out.println("#############################");
            System.out.println("No duplicate entries allowed!");
            System.out.println("#############################");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void updateInvoice(int id, Date date, String customer, String description, double value, Boolean paid) {
        try {
            PreparedStatement ps = Connection().prepareStatement(UPDATE);
            ps.setDate(1, date);
            ps.setString(2, customer);
            ps.setString(3, description);
            ps.setDouble(4, value);
            ps.setBoolean(5, paid);
            ps.setInt(6, id);
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void deleteInvoice(int id) {
        try {
            PreparedStatement ps = Connection().prepareStatement(DELETE);
            ps.setInt(1, id);
            ps.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
